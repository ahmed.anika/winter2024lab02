import java.util.Scanner;
public class GameLauncher{
	public static void main (String [] args){
        java.util.Scanner reader = new java.util.Scanner(System.in);
		int gameChosen = 0;
		while (gameChosen != 1 || gameChosen != 2){
			if (gameChosen == 1){
				//Get user input
				System.out.println("Enter a 4-letter word:");
				String word = reader.next();
				//Convert to upper case
				word = word.toUpperCase();
				//Start hangman game
				Hangman.runGame(word);	
				break;
				
			}else if(gameChosen == 2){
				Wordle.runGame(Wordle.generateWord());
				break;				
			} else {
				System.out.println("Which game would you like to play Hangman(1) or Wordle(2)");
				gameChosen = reader.nextInt();
				reader.nextLine();	
			}
		}
	}
}
	
