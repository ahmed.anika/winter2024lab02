import java.util.Scanner;
import java.util.Random;

/* 
* assignment 3 2023 - 110
* 
* program  to create the game Wordle
* five-letter word has to be guessed by the
* player, the colour of letter of the guessed word will 
* indicate if the letter is in the correct posiition or 
* even present in the word that has to be guessed
* @author Anika Ahmed student # 2333627
* @version 2023-12
*/ 

public class Wordle {
   /* public static void main(String[] args) {
    /*
    *  method will generate a word with the help of 
    *  the generatedWord() method
    * Then it will run the game 
    *
        generateWord();
        runGame(generateWord());
    } */

    public static String generateWord() {
    // list of 5-letter words where one will be selected randomly as the word the player will have to guess
        String[] wordList = {"LASER", "FIRST", "PRICE", "PHONE", "FROST",
                "HAZEL", "VENOM", "NOVEL", "IMAGE", "WALTZ", "DANCE", "IVORY", "NIGHT",
                "FLAME", "CHARM", "LUCKY", "CUTIE", "THIEF", "LUNAR", "SOLAR"};
        Random randGen = new Random();
        int wordGenerator = randGen.nextInt(wordList.length);
        return wordList[wordGenerator];
    }

    public static boolean letterInWord(String word, char letter) {
    // method checks if the letters in the guessed word is present the word that has to be guessed
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                return true;
            }
        }
        return false;
    }

    public static boolean letterInSlot(String word, char letter, int position) {
    // method checks if the letters in the guessed word is in the same postion as the letters of the word that has to be guessed
    if (word.charAt(position) == letter) {    
            return true;
        }
        return false;
    }

    public static String[] guessWord(String answer, String guess) {
    /* 
    * method creates an array of colours so with the help of the method presentResults
    * the letters of the word guessed will indicate if each letter
    * is present in the word and if so is it in the right position.
    */
    String[] colours = new String[guess.length()];

    for (int i = 0; i < guess.length(); i++) {
        char guessedLetter = guess.charAt(i);
        boolean correctLetter = letterInWord(answer, guessedLetter);
        boolean correctPosition = letterInSlot(answer, guessedLetter, i);

             if (correctPosition && correctLetter) {
                colours[i] = "green";
             } else if (!correctPosition && correctLetter) {
                colours[i] = "yellow";
             } else {
                colours[i] = "white";
             }
        }
         return colours;
    }

   public static void presentResults(String word, String[] colours) {
    /* 
    * As mentioned in the previous comment, this method changes the colour of 
    * the letters of the word guessed to indicate if the letter
    * is present in the letter if so is it in the right position
    */
        for (int i = 0; i < colours.length; i++) {
            char letter = word.charAt(i);

            if ("green".equals(colours[i])) {
                System.out.print("\u001B[32m" + letter + "\u001B[0m"); // letter turns green 
            } else if ("yellow".equals(colours[i])) {
                System.out.print("\u001B[33m" + letter + "\u001B[0m"); // letter turns yellow
            } else if ("white".equals(colours[i])) {
                System.out.print("\u001B[0m" + letter); // letter turns white
            } 
        }System.out.println (""); //  the text line prints on another line and not like FROSTEnter your guess:

    }

    public static String readGuess() {
    // method verifies if the word the player is five letters long if not it will ask again for valid guess
        java.util.Scanner reader = new java.util.Scanner(System.in);
        String guess = "";

        while (guess.length() != 5) {
		    System.out.println("Enter your guess:");
		    guess = reader.next().toUpperCase();
			if (guess.length() != 5) {
			System.out.println("Invalid guess");
           }
        }
            return guess;
    }

    public static void runGame(String word) {
    /*
    * By calling the previous methods,this method builds the game to 
    * ask the player to guess a 5 letter word 
    * Then it displays the guessed word with each letter with the appropriate colour
    * if the word is guessed it prints out "you win" and the game ends
    * if after 6 tries, the word still have not been guessed, 
    * it prints out "Try again" in the end of the game
    */
        word = generateWord();
        int guessCount = 0;

        while (guessCount < 6) {
            String guess = readGuess();
            String[] colours = guessWord(word, guess);
            presentResults(guess, colours);
            
            if (guess.equals(word)) {
                System.out.println("You win!");
                guessCount = 6; // makes the loop end if they win 
            } else if (guessCount == 5) {
                System.out.println("Try again");
            }
            guessCount++;
        }
    }
} 


         


